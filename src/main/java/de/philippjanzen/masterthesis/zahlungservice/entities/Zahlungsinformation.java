package de.philippjanzen.masterthesis.zahlungservice.entities;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.philippjanzen.masterthesis.zahlungservice.zahlung.Zahlung;

@Entity
public class Zahlungsinformation {

	@Id
	private String id;
	private String iban;
	private String bic;
	private String kontoinhaber;
	private String bank;

	@OneToOne
	@PrimaryKeyJoinColumn
	private Zahlung zahlung;

	protected Zahlungsinformation() {
	}

	public Zahlungsinformation(String iban, String bic, String kontoinhaber, String bank) {
		this.iban = iban;
		this.bic = bic;
		this.kontoinhaber = kontoinhaber;
		this.bank = bank;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getKontoinhaber() {
		return kontoinhaber;
	}

	public void setKontoinhaber(String kontoinhaber) {
		this.kontoinhaber = kontoinhaber;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	@JsonIgnore
	public boolean isValid() {
		return !((iban == null || iban.isEmpty()) || (bic == null || bic.isEmpty())
				|| (kontoinhaber == null || kontoinhaber.isEmpty()) || (bank == null || bank.isEmpty()));
	}

	@PrePersist
	private void generateId() {
		if (id == null || id.isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}

}
