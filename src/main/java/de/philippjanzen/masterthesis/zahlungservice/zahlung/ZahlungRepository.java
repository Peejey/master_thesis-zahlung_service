package de.philippjanzen.masterthesis.zahlungservice.zahlung;

import org.springframework.data.repository.CrudRepository;

public interface ZahlungRepository extends CrudRepository<Zahlung, String> {

}
