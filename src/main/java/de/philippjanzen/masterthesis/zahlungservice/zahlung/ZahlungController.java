package de.philippjanzen.masterthesis.zahlungservice.zahlung;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;
import de.philippjanzen.masterthesis.zahlungservice.dtos.ZahlungValidationDTO;
import de.philippjanzen.masterthesis.zahlungservice.entities.Zahlungsinformation;

@RestController
public class ZahlungController {

	private ZahlungService zahlungService;

	public ZahlungController(ZahlungService zahlungService) {
		this.zahlungService = zahlungService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/zahlung")
	public String zahlen(@RequestBody ZahlungsinformationDTO zahlungsinformationDTO) {
		return zahlungService.zahlen(zahlungsinformationDTO);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/zahlung")
	public List<Zahlung> liesAlleZahlungen() {
		return zahlungService.liesAlleZahlungen();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/zahlung/{id}")
	public ZahlungValidationDTO stornieren(@PathVariable String id) {
		return zahlungService.stornieren(id);
	}
}