package de.philippjanzen.masterthesis.zahlungservice.zahlung;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import org.springframework.data.annotation.Transient;

import de.philippjanzen.masterthesis.sharedservice.events.DomainAggregate;
import de.philippjanzen.masterthesis.zahlungservice.entities.Zahlungsinformation;
import de.philippjanzen.masterthesis.zahlungservice.events.domainevents.ZahlungEvent;
import de.philippjanzen.masterthesis.zahlungservice.events.domainevents.ZahlungGetaetigtEvent;
import de.philippjanzen.masterthesis.zahlungservice.events.domainevents.ZahlungStorniertEvent;

@Entity
public class Zahlung extends DomainAggregate {

	@Id
	private String id;
	@OneToOne(cascade = CascadeType.ALL)
	private Zahlungsinformation zahlungsinformation;
	private double zahlungssumme;
	private String referenzId;
	private boolean bezahlt;
	private String bezahltTimestamp;
	private boolean storniert;
	private String storniertTimestamp;

	protected Zahlung() {
	}

	public Zahlung(Zahlungsinformation zahlungsinformation) {
		this.zahlungsinformation = zahlungsinformation;
		generateId();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Zahlungsinformation getZahlungsinformation() {
		return zahlungsinformation;
	}

	public void setZahlungsinformation(Zahlungsinformation zahlungsinformation) {
		this.zahlungsinformation = zahlungsinformation;
	}

	public double getZahlungssumme() {
		return zahlungssumme;
	}

	public void setZahlungssumme(double zahlungssumme) {
		this.zahlungssumme = zahlungssumme;
	}

	public String getReferenzId() {
		return referenzId;
	}

	public void setReferenzId(String referenzId) {
		this.referenzId = referenzId;
	}

	public String getBezahltTimestamp() {
		return bezahltTimestamp;
	}

	public void setBezahltTimestamp(LocalDateTime bezahltTimestamp) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		this.bezahltTimestamp = bezahltTimestamp.format(formatter).toString();
	}

	public String getStorniertTimestamp() {
		return storniertTimestamp;
	}

	public void setStorniertTimestamp(LocalDateTime storniertTimestamp) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		this.storniertTimestamp = storniertTimestamp.format(formatter).toString();
	}

	public boolean isBezahlt() {
		return bezahlt;
	}

	public void setBezahlt(boolean bezahlt) {
		this.bezahlt = bezahlt;
	}

	public boolean isStorniert() {
		return storniert;
	}

	public void setStorniert(boolean storniert) {
		this.storniert = storniert;
	}

	public void zahlen() {
		bezahlt = true;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		bezahltTimestamp = LocalDateTime.now().format(formatter).toString();
	}

	public ZahlungEvent zahlenWithEvents() {
		bezahlt = true;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		bezahltTimestamp = LocalDateTime.now().format(formatter).toString();
		return new ZahlungGetaetigtEvent(id);
	}

	public void stornieren() {
		storniert = true;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		storniertTimestamp = LocalDateTime.now().format(formatter).toString();
	}

	public ZahlungEvent stornierenWithEvents() {
		storniert = true;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		storniertTimestamp = LocalDateTime.now().format(formatter).toString();
		return new ZahlungStorniertEvent();
	}

	@PrePersist
	private void generateId() {
		if (id == null || id.isEmpty()) {
			setId(UUID.randomUUID().toString());
		}
	}
}
