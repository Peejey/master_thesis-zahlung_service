package de.philippjanzen.masterthesis.zahlungservice.zahlung;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventPublisher;
import de.philippjanzen.masterthesis.zahlungservice.dtos.ZahlungValidationDTO;
import de.philippjanzen.masterthesis.zahlungservice.entities.Zahlungsinformation;
import de.philippjanzen.masterthesis.zahlungservice.events.domainevents.ZahlungEvent;
import de.philippjanzen.masterthesis.zahlungservice.mapper.DTOMapper;

@Service
public class ZahlungService {

	private ZahlungRepository zahlungRepository;
	private DTOMapper dtoMapper;

	@Autowired
	private DomainEventPublisher<ZahlungEvent> domainEventPublisher;

	public ZahlungService(ZahlungRepository zahlungRepository, DTOMapper dtoMapper) {
		this.zahlungRepository = zahlungRepository;
		this.dtoMapper = dtoMapper;
	}

	@Transactional
	public String zahlen(ZahlungsinformationDTO zahlungsinformationDTO) {

		Zahlung zahlung = dtoMapper.mappeZahlung(zahlungsinformationDTO);
		zahlung.zahlen();
		if (zahlungsinformationDTO.getIban().contains("fehler")) {
			throw new IllegalArgumentException();
		}
		return zahlungRepository.save(zahlung).getId();
	}

	public List<Zahlung> liesAlleZahlungen() {
		List<Zahlung> zahlungen = new ArrayList<>();
		zahlungRepository.findAll().forEach(zahlungen::add);
		return zahlungen;
	}

	@Transactional
	public ZahlungValidationDTO stornieren(String zahlungId) {
		Zahlung zahlung = zahlungRepository.findById(zahlungId).get();
		zahlung.stornieren();
		zahlungRepository.save(zahlung);
		ZahlungValidationDTO response = new ZahlungValidationDTO();
		response.setValid(true);
		return response;
	}

	@Transactional
	public void stornierenWithEvents(String zahlungId, String referenceId) {
		Zahlung zahlung = zahlungRepository.findById(zahlungId).get();
		ZahlungEvent event = zahlung.stornierenWithEvents();
		zahlungRepository.save(zahlung);
		domainEventPublisher.publish(Zahlung.class, zahlung.getId(), referenceId, Collections.singletonList(event));
	}

	@Transactional
	public String zahlen(Zahlungsinformation zahlungsinformation) {
		Zahlung zahlung = new Zahlung(zahlungsinformation);
		zahlung.zahlen();
		if (zahlungsinformation.getIban().contains("fehler")) {
			throw new IllegalArgumentException();
		}
		return zahlungRepository.save(zahlung).getId();
	}

	@Transactional
	public void zahlenWithEvents(Zahlungsinformation zahlungsinformation, String referenceId) {
		// Debugging / Demo Hintergrund
		if (zahlungsinformation.getIban().contains("fehler")) {
			throw new IllegalArgumentException();
		}
		Zahlung zahlung = new Zahlung(zahlungsinformation);
		ZahlungEvent event = zahlung.zahlenWithEvents();
		zahlungRepository.save(zahlung).getId();
		domainEventPublisher.publish(Zahlung.class, zahlung.getId(), referenceId, Collections.singletonList(event));
	}
}
