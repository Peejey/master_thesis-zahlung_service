package de.philippjanzen.masterthesis.zahlungservice.events.domainevents.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.philippjanzen.masterthesis.zahlungservice.events.domainevents.BestellungAbgebrochenEvent;
import de.philippjanzen.masterthesis.zahlungservice.events.domainevents.BestellungAufgegebenEvent;
import de.philippjanzen.masterthesis.zahlungservice.zahlung.ZahlungService;

@Component
public class BestellungEventConsumer {

	@Autowired
	ZahlungService zahlungService;

	public BestellungEventConsumer() {
	}

	public void accept(BestellungAufgegebenEvent bestellungAufgegebenEvent, String aggregateId, String correlationId) {
		zahlungService.zahlenWithEvents(bestellungAufgegebenEvent.getZahlungsinformation(), aggregateId);
	}

	public void accept(BestellungAbgebrochenEvent bestellungAbgebrochenEvent, String aggregateId,
			String correlationId) {
		zahlungService.stornierenWithEvents(bestellungAbgebrochenEvent.getZahlungId(), aggregateId);
	}

}
