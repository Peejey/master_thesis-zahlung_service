package de.philippjanzen.masterthesis.zahlungservice.events;

import java.io.IOException;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.zahlungservice.events.domainevents.BestellungAufgegebenEvent;
import de.philippjanzen.masterthesis.zahlungservice.zahlung.ZahlungService;

@Component
public class BestellungEventsSubscriber implements Subscriber<DomainEventEnvelope> {

	private ObjectMapper objectMapper;
	private BestellungEventsConsumer bestellungEventsConsumer;

	public BestellungEventsSubscriber(ZahlungService zahlungService, ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
		this.bestellungEventsConsumer = new BestellungEventsConsumer(zahlungService);
	}

	@Override
	public void onSubscribe(Subscription subscription) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNext(DomainEventEnvelope item) {
		if (item.getAggregateType().equals("de.philippjanzen.masterthesis.bestellungservice.entities.Bestellung")) {
			if (item.getEventType()
					.equals("de.philippjanzen.masterthesis.bestellungservice.events.BestellungAufgegebenEvent")) {
				try {
					BestellungAufgegebenEvent bestellungAufgegebenEvent = objectMapper.readValue(item.getEvent(),
							BestellungAufgegebenEvent.class);
					bestellungEventsConsumer.accept(bestellungAufgegebenEvent, item.getAggregateId(),
							item.getCorrelationId());
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub

	}

}
