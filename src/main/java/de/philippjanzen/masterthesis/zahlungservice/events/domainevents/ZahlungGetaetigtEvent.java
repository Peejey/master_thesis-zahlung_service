package de.philippjanzen.masterthesis.zahlungservice.events.domainevents;

public class ZahlungGetaetigtEvent extends ZahlungEvent {

	private String id;

	public ZahlungGetaetigtEvent(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
