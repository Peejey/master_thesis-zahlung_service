package de.philippjanzen.masterthesis.zahlungservice.events.domainevents;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEvent;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;

public class ZahlungEvent extends DomainEvent {

	@Override
	public String getChannel() {
		return EventsConnectionConfig.ZAHLUNG_SERVICE_CHANNEL;
	}

}
