package de.philippjanzen.masterthesis.zahlungservice.events.domainevents;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BestellungAbgebrochenEvent {

	@JsonProperty
	private String zahlungId;

	protected BestellungAbgebrochenEvent() {
	}

	public String getZahlungId() {
		return zahlungId;
	}

}
