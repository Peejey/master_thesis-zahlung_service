package de.philippjanzen.masterthesis.zahlungservice.events;

import de.philippjanzen.masterthesis.zahlungservice.events.domainevents.BestellungAufgegebenEvent;
import de.philippjanzen.masterthesis.zahlungservice.zahlung.ZahlungService;

public class BestellungEventsConsumer {

	private ZahlungService zahlungService;

	public BestellungEventsConsumer(ZahlungService zahlungService) {
		this.zahlungService = zahlungService;
	}

	public void accept(BestellungAufgegebenEvent bestellungAufgegebenEvent, String aggregateId, String correlationId) {
		zahlungService.zahlen(bestellungAufgegebenEvent.getZahlungsinformation());
	}
}
