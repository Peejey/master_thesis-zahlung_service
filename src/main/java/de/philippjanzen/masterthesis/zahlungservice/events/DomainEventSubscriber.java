package de.philippjanzen.masterthesis.zahlungservice.events;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;
import de.philippjanzen.masterthesis.zahlungservice.zahlung.ZahlungService;

@Component
public class DomainEventSubscriber {

	private ChannelManager<DomainEventEnvelope> channelManager;

	private ZahlungService zahlungService;

	private ObjectMapper objectMapper;

	public DomainEventSubscriber(ZahlungService zahlungService, ObjectMapper objectMapper,
			ChannelManager<DomainEventEnvelope> channelManager) {
		this.channelManager = channelManager;
		this.zahlungService = zahlungService;
		this.objectMapper = objectMapper;
		subscribeToAllChannels();
	}

	private void subscribeToAllChannels() {
		channelManager.subscribe(new BestellungEventsSubscriber(zahlungService, objectMapper),
				Collections.singleton(EventsConnectionConfig.BESTELLUNG_SERVICE_CHANNEL));
	}

}
