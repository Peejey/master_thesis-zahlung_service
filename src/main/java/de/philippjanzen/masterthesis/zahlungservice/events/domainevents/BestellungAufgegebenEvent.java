package de.philippjanzen.masterthesis.zahlungservice.events.domainevents;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.philippjanzen.masterthesis.zahlungservice.entities.Zahlungsinformation;

public class BestellungAufgegebenEvent {

	@JsonProperty
	private double gesamtsumme;
	@JsonProperty
	private String bestellDatum;
	@JsonProperty
	private Zahlungsinformation zahlungsinformation;

	protected BestellungAufgegebenEvent() {
	}

	public double getGesamtsumme() {
		return gesamtsumme;
	}

	public String getBestellDatum() {
		return bestellDatum;
	}

	public Zahlungsinformation getZahlungsinformation() {
		return zahlungsinformation;
	}

}
