package de.philippjanzen.masterthesis.zahlungservice.mapper;

import org.springframework.stereotype.Service;

import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;
import de.philippjanzen.masterthesis.zahlungservice.entities.Zahlungsinformation;
import de.philippjanzen.masterthesis.zahlungservice.zahlung.Zahlung;

@Service
public class DTOMapper {

	public Zahlung mappeZahlung(ZahlungsinformationDTO zahlungsinformationDTO) {
		Zahlung zahlung = new Zahlung(mappeZahlungsinformation(zahlungsinformationDTO));
		zahlung.setZahlungssumme(zahlungsinformationDTO.getZahlungsSumme());
		zahlung.setReferenzId(zahlungsinformationDTO.getReferenzId());
		return zahlung;
	}

	private Zahlungsinformation mappeZahlungsinformation(ZahlungsinformationDTO zahlungsinformationDTO) {
		return new Zahlungsinformation(zahlungsinformationDTO.getIban(), zahlungsinformationDTO.getBic(),
				zahlungsinformationDTO.getKontoinhaber(), zahlungsinformationDTO.getBank());
	}

}
