package de.philippjanzen.masterthesis.zahlungservice.saga.handlers;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;

import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestand;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.AktualisiereLagerbestandZurueck;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.LeereWarenkorb;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.LeereWarenkorbZurueck;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.SagaCustomCommandHandlersBuilder;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.TaetigeGutschrift;
import de.philippjanzen.masterthesis.sharedservice.saga.commands.TaetigeZahlung;
import de.philippjanzen.masterthesis.sharedservice.saga.producers.SagaCustomReplyMessageBuilder;
import de.philippjanzen.masterthesis.sharedservice.saga.replies.TaetigeZahlungAntwort;
import de.philippjanzen.masterthesis.zahlungservice.zahlung.Zahlung;
import de.philippjanzen.masterthesis.zahlungservice.zahlung.ZahlungService;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;
import io.eventuate.tram.commands.common.CommandMessageHeaders;
import io.eventuate.tram.commands.consumer.CommandHandlers;
import io.eventuate.tram.commands.consumer.CommandMessage;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.sagas.participant.SagaCommandHandlersBuilder;
import io.eventuate.tram.sagas.participant.SagaReplyMessageBuilder;

public class ZahlungCommandHandlers {

	@Autowired
	private ZahlungService zahlungService;
	@Autowired
	private IdGenerator idGenerator;

	public ZahlungCommandHandlers() {
	}

	public CommandHandlers commandHandlers() {
		return SagaCustomCommandHandlersBuilder.fromChannel(SagaConnectionConfig.ZAHLUNG_SERVICE_CHANNEL)
				.onMessage(TaetigeZahlung.class, this::taetigeZahlung)
				.onMessage(TaetigeGutschrift.class, this::taetigeGutschrift).build();
//		return SagaCommandHandlersBuilder.fromChannel(SagaConnectionConfig.ZAHLUNG_SERVICE_CHANNEL)
//				.onMessage(TaetigeZahlung.class, this::taetigeZahlung)
//				.onMessage(TaetigeGutschrift.class, this::taetigeGutschrift).build();
	}

	@Transactional
	public Message taetigeZahlung(CommandMessage<TaetigeZahlung> cm) {
		ZahlungsinformationDTO zahlungsInformation = cm.getCommand().getZahlung();
		String zahlungId = zahlungService.zahlen(zahlungsInformation);
		return new SagaCustomReplyMessageBuilder().withSuccess(new TaetigeZahlungAntwort(zahlungId))
				.withId(idGenerator.genId().asString()).withLock(Zahlung.class, zahlungId).build();
	}

	@Transactional
	public Message taetigeGutschrift(CommandMessage<TaetigeGutschrift> cm) {
		String zahlungId = cm.getCommand().getZahlungId();
		zahlungService.stornieren(zahlungId);
		return new SagaCustomReplyMessageBuilder().withSuccess().withId(idGenerator.genId().asString()).build();
	}
}
