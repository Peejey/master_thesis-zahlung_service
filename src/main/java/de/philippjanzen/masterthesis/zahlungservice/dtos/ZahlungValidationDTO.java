package de.philippjanzen.masterthesis.zahlungservice.dtos;

public class ZahlungValidationDTO {
	boolean valid;

	public ZahlungValidationDTO() {

	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
